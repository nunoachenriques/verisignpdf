#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
VeriSignPDF

Command-line interface.
"""

import argparse
import logging

from endesive import pdf


class Cli(object):
    """
    Provides bootstrap and complete integration of application run from
    command-line in a Python script, such as:

        from verisignpdf.cli import Cli


        if __name__ == "__main__":
            Cli().bootstrap().run()
    """

    def __init__(self):
        """
        Initialisation of the class parameters:

            * self.pdf_file_name: local pdf_file_name to check signature.
        """
        self.pdf_file_name = None
        with open("VERSION") as f:
            self.version = f.read()

    def bootstrap(self) -> "Cli":
        """
        Required to protect the start up and help the user. Parse the
        command-line arguments to obtain the pdf_file_name data and options. Set the
        logging level regarding verbosity requested.

        :return: This instance with self.pdf_file_name = args.pdf_file_name.
        """
        # noinspection PyTypeChecker
        cmd_line_parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
        cmd_line_parser.description = \
            f"VeriSignPDF {self.version}" \
            "\n\nhttps://gitlab.com/nunoachenriques/verisignpdf"
        cmd_line_parser.add_argument("-v", action="count", default=0,
                                     help="Output verbosity: none, info (-v), debug (-vv).")
        cmd_line_parser.add_argument("pdf_file_name", type=str,
                                     help="The file path containing the PDF document.")
        cmd_line_parser.epilog = \
            "Usage examples:" \
            "\n\n  Inspect PDF, output the verification result:" \
            "\n    pipenv run python app_cli.py -v test/dummy.pdf" \
            "\n\n  Inspect PDF, output the verification result:" \
            "\n    pipenv run python app_cli.py -v test/dummy-signed.pdf"
        args = cmd_line_parser.parse_args()
        if args.v == 0:
            logging.getLogger().setLevel(logging.CRITICAL)
        elif args.v == 1:
            logging.getLogger().setLevel(logging.INFO)
        else:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.debug(f"Logging set to {logging.getLevelName(logging.getLogger().level)}")
        self.pdf_file_name = args.pdf_file_name
        return self

    def run(self):
        """
        Run the application, get data and parse from given pdf_file_name, and output
        the result of signature verification.
        """
        hash_ok: bool = False
        signature_ok: bool = False
        try:
            with open(self.pdf_file_name, "rb") as pdf_file_io:
                (hash_ok, signature_ok, _) = pdf.verify(pdf_file_io.read())
            logging.info(f"OK! Hash: {hash_ok} | Signature: {signature_ok}"
                         if hash_ok and signature_ok else "FAILED!")
        except IOError as e:
            logging.error(f"FAILED! {e}")
        except AssertionError:
            logging.error("FAILED! Hint: file tampered?")
        raise SystemExit(0 if hash_ok and signature_ok else 1)
