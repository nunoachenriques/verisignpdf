# VeriSignPDF

Verification of digital signatures in PDF documents.

## Install

The tools required to compile dependency packages and create the virtual
environment. Moreover, `flake8` and `bandit` for quality assurance regarding
code style and security issues.

```bash
apt install swig g++
sudo pip3 install pipenv
```

Inside the `verisignpdf` project directory, install dependencies, creating the
environment. Moreover, it install `pre-commit` into the project's `.git`
hooks --- it will keep you safe from usual code style or security mistakes when
committing changes.

```bash
pipenv install --dev
pipenv run pre-commit install
```

## Check

There is a command-line **quality assurance** test suite. It provides code
style (`flake8`), security (`bandit`), and unit testing.

```bash
./qa.sh
```

## Run

### Command-line interface

A command-line interface is available and ready to use by running `app_cli.py`.

```bash
pipenv run python app_cli.py -h
```

## Deploy

```bash
TODO
```
