#  Copyright 2020 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``cli`` module.
This is an integration test.
"""

import os
import subprocess  # nosec | Avoids Bandit B404 check
import sys
import unittest

from verisignpdf.cli import Cli


class TestCli(unittest.TestCase):

    def test_bootstrap_run(self):
        with self.assertRaises(SystemExit) as cm:
            # simulate argument from command line
            sys.argv[1] = os.path.join(os.path.dirname(__file__), "dummy-signed.pdf")
            Cli().bootstrap().run()
        # Exit code should be 0, it's a properly signed PDF
        self.assertTrue(cm.exception.code == 0, "CLI FAILED!")

    def test_cli(self):
        # Not signed
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py",
                                 os.path.join(os.path.dirname(__file__), "dummy.pdf")],
                                capture_output=True)
        self.assertEqual(1, result.returncode, f"Return code [{result.returncode}] FAILED!")
        # Signed
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py",
                                 os.path.join(os.path.dirname(__file__), "dummy-signed.pdf")],
                                capture_output=True)
        self.assertEqual(0, result.returncode, f"Return code [{result.returncode}] FAILED!")
        # Tampered
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py",
                                 os.path.join(os.path.dirname(__file__), "dummy-signed-hacked.pdf")],
                                capture_output=True)
        self.assertEqual(1, result.returncode, f"Return code [{result.returncode}] FAILED!")
        # Signed sequentially by two persons
        result = subprocess.run(["pipenv", "run",  # nosec | Avoids Bandit B603, B607 check
                                 "python", "app_cli.py",
                                 os.path.join(os.path.dirname(__file__), "dummy-signed-signed.pdf")],
                                capture_output=True)
        self.assertEqual(0, result.returncode, f"Return code [{result.returncode}] FAILED!")


if __name__ == "__main__":
    unittest.main()
